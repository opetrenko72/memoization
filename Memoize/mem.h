#pragma once

template<class I, class O>
std::function<O(I)> memoize(std::function<O(I)> f) 
{
	std::map<typename std::decay<I>::type, O> memos;
	return [=](I i) mutable {
		auto it = memos.lower_bound(i);
		if(it == memos.end() || it->first != i)
			it = memos.insert(it, std::make_pair(i, f(i)));
		return it->second;
	};
}