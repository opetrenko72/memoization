// Memoize.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "mem.h"
#include <ctime>

template<class F>
void time_func(F f, int N)
{
	auto start = clock();
	for(int i = 0; i < N; ++i)
		std::cout << i << '\t' << f(i) << '\n';
	std::cout << "Time elapsed: " << (clock() - start) * 1.0 / CLOCKS_PER_SEC << '\n';
}

int _tmain(int argc, _TCHAR* argv[])
{
	std::function<long(int)> fib;

	fib = [&](int i) {
		if(i <= 1)
			return (long)1;
		else
			return fib(i - 1) + fib(i - 2);
	};

	// Before memoization
	time_func(fib, 40);

	fib = memoize(fib);
	// After memoization
	time_func(fib, 40);

	return 0;
}

