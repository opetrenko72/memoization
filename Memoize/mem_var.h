#pragma once

template<class... I, class O>
std::function<O(I...)> memoize_var(std::function<O(I...)> f) 
{
	std::map<std::tuple<typename std::decay<I>::type...>, O> memos;
	return [=](I... i) mutable {
		auto args = std::tie(i...);
		auto it = memos.lower_bound(args);
		if(it == memos.end() || it->first != args)
			it = memos.insert(it, std::make_pair(args, f(i...)));
		return it->second;
	};
}
